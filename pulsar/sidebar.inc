            <script src="assets/js/jquery-3.2.1.min.js" type="text/javascript"></script>
            <script src="assets/js/menu.js" type="text/javascript"></script>
            <!-- SIDEBAR - START -->
            <div class="page-sidebar ">

                <!-- MAIN MENU - START -->
                <div class="page-sidebar-wrapper" id="main-menu-wrapper"> 

                    <!-- USER INFO - START -->
                    <div class="profile-info row">

                        <div class="profile-image col-lg-4 col-md-4 col-4">
                            <a href="ui-profile.html">
                                <img src="data/profile/<?php echo $_SESSION['img']; ?>" class="img-fluid rounded-circle">
                            </a>
                        </div>

                        <div class="profile-details col-lg-8 col-md-8 col-8">

                            <h3>
                                <a href="ui-profile.html#"><?php echo $_SESSION['username']; ?></a>

                                <!-- Available statuses: online, idle, busy, away and offline -->
                                <span class="profile-status online"></span>
                            </h3>

                            <p class="profile-title">Administrator</p>

                        </div>

                    </div>
                    <!-- USER INFO - END -->



                    <ul class='wraplist'>	


                        <li class=""> 
                            <a href="index.php">
                                <i class="fa fa-dashboard"></i>
                                <span class="title">Dashboard</span>
                            </a>
                        </li>
                        <!--
                        <li class=""> 
                            <a href="javascript:;">
                                <i class="fa fa-envelope"></i>
                                <span class="title">Messaging</span>
                                <span class="arrow "></span><span class="badge badge-orange">4</span>
                            </a>
                            <ul class="sub-menu" >
                                <li>
                                    <a class="" href="soc-mail-inbox.html" >Inbox</a>
                                </li>
                                <li>
                                    <a class="" href="soc-mail-compose.html" >Compose</a>
                                </li>
                                <li>
                                    <a class="" href="soc-mail-view.html" >View</a>
                                </li>
                            </ul>
                        </li>
                        <li class=""> 
                            <a href="soc-activity.html">
                                <i class="fa fa-comments"></i>
                                <span class="title">Activity</span>
                            </a>
                        </li>
                        <li class=""> 
                            <a href="javascript:;">
                                <i class="fa fa-user"></i>
                                <span class="title">Members</span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu" >
                                <li>
                                    <a class="" href="soc-members.html" >All Members</a>
                                </li>
                                <li>
                                    <a class="" href="soc-member-add.html" >Add Member</a>
                                </li>
                                <li>
                                    <a class="" href="soc-member-edit.html" >Edit Member</a>
                                </li>
                                <li>
                                    <a class="" href="soc-member-profile.html" >Member Profile</a>
                                </li>
                            </ul>
                        </li>
                        <li class=""> 
                            <a href="javascript:;">
                                <i class="fa fa-upload"></i>
                                <span class="title">Media</span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu" >
                                <li>
                                    <a class="" href="soc-media.html" >All Media</a>
                                </li>
                                <li>
                                    <a class="" href="soc-upload.html" >Upload</a>
                                </li>
                            </ul>
                        </li>
                        <li class=""> 
                            <a href="javascript:;">
                                <i class="fa fa-group"></i>
                                <span class="title">Groups</span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu" >
                                <li>
                                    <a class="" href="soc-groups.html" >All Groups</a>
                                </li>
                                <li>
                                    <a class="" href="soc-group-add.html" >Add Group</a>
                                </li>
                                <li>
                                    <a class="" href="soc-group-edit.html" >Edit Group</a>
                                </li>
                                <li>
                                    <a class="" href="soc-group-view.html" >View Group</a>
                                </li>
                            </ul>
                        </li>
                        <li class=""> 
                            <a href="javascript:;">
                                <i class="fa fa-lock"></i>
                                <span class="title">Access pages</span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu" >
                                <li>
                                    <a class="" href="ui-login.html" >Login</a>
                                </li>
                                <li>
                                    <a class="" href="ui-register.html" >Registration</a>
                                </li>
                                <li>
                                    <a class="" href="ui-lockscreen.html" >Lock Screen</a>
                                </li>
                            </ul>
                        </li>
                    -->
                        <li class=""> 
                            <a href="javascript:;">
                                <i class="fa fa-bar-chart"></i>
                                <span class="title">Twitter Reports</span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu" >
                                <li>
                                    <a class="" href="rpt_wordcloud.php" >WordCloud</a>
                                </li>
                                <li>
                                    <a class="" href="rpt_account_grow.php" >Followers</a>
                                </li>         
                                <li>
                                    <a class="" href="rpt_volume_per_user.php" >Volume per user</a>
                                </li>                                                              
                                <li>
                                    <a class="" href="rpt_volume.php" >Twitter Volume</a>
                                </li>          
                                <li>
                                    <a class="" href="rpt_top_accounts.php" >Top Tweets</a>
                                </li>                                                           
                            </ul>
                        </li>
                        <!--<li class=""> 
                            <a href="soc-friends.html">
                                <i class="fa fa-smile-o"></i>
                                <span class="title">Friends</span>
                            </a>
                        </li>-->
                        <li class=""> 
                            <a href="blo-user-edit.php">
                                <i class="fa fa-cogs"></i>
                                <span class="title">Settings</span>
                            </a>
                        </li>

                    </ul>

                </div>
                <!-- MAIN MENU - END -->



                <div class="project-info">

                    <div class="block1">
                        <div class="data">
                            <span class='title'>Users</span>
                            <span class='total'>7745</span>
                        </div>
                        <div class="graph">
                            <span class="sidebar_orders">...</span>
                        </div>
                    </div>

                    <div class="block2">
                        <div class="data">
                            <span class='title'>Uptime</span>
                            <span class='total'>99%</span>
                        </div>
                        <div class="graph">
                            <span class="sidebar_visitors">...</span>
                        </div>
                    </div>

                </div>



            </div>
            <!--  SIDEBAR - END -->

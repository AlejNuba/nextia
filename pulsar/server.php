<?php
session_start();

// initializing variables
$email2 = "";
$nombre = "";
$apellido = "";
$username = "";
$email    = "";
$errors = array(); 

// connect to the database
//$link = mysqli_connect('localhost', 'root', '', 'registration');
include('../config/configWS.php');
// REGISTER USER
if (isset($_POST['reg_user'])) {
  // receive all input values from the form
  $nombre =   mysqli_real_escape_string($link, $_POST['nombre']);
  $apellido =   mysqli_real_escape_string($link, $_POST['apellido']);
  $username =   mysqli_real_escape_string($link, $_POST['username']);
  $email =      mysqli_real_escape_string($link, $_POST['email']);
  $password_1 = mysqli_real_escape_string($link, $_POST['password_1']);
  $password_2 = mysqli_real_escape_string($link, $_POST['password_2']);

  $nombre =  trim($nombre);
  $apellido =  trim($apellido);
  $username = trim($username);
  $email = trim($email);
  $password_1 = trim($password_1);
  $password_2 = trim($password_2);


  // form validation: ensure that the form is correctly filled ...
  // by adding (array_push()) corresponding error unto $errors array
  if (empty($nombre)) { array_push($errors, "What's your name?"); }
  if (empty($apellido)) { array_push($errors, "What's your last name?"); }
  if (empty($username)) { array_push($errors, "What's your username "); }

  $cadena_buscada   = '@';
  $posicion_coincidencia = strpos($email, $cadena_buscada);
  if (empty($email)) {
    array_push($errors, "Please enter a valid email address.");
  } else {
    
      if ($posicion_coincidencia === false) {
        array_push($errors, "Your email does not belong to narrative.tech ");

      } else {

        $email2 = $email;
        }
        
  }
  if (empty($password_1)) { array_push($errors, "Password is required"); }
  if ($password_1 != $password_2) {
	array_push($errors, "The two passwords do not match");
  }

  // first check the database to make sure 
  // a user does not already exist with the same username and/or email


  $user_check_query = "SELECT * FROM nextia_user WHERE username='$username' OR email='$email2' LIMIT 1";
  $result = mysqli_query($link, $user_check_query);
  $user = mysqli_fetch_assoc($result);
  
  if ($user) { // if user exists
    if ($user['username'] === $username) {
      array_push($errors, "Username already exists");
    }

    if ($user['email'] === $email2) {
      array_push($errors, "email already exists");
    }
  }

  // Finally, register user if there are no errors in the form
  if (count($errors) == 0) {
    $password = md5($password_1);//encrypt the password before saving in the database
    $imgper = '1/1.png';

  	$query = "INSERT INTO nextia_user (id_company, username, email, password, name, last_name, user_image) 
          VALUES('1','$username', '$email2', '$password', '$nombre', '$apellido', '$imgper')";
          echo $query;
  	mysqli_query($link, $query);


    $_SESSION['id'] = $id;
    $_SESSION['email'] = $email2;
    $_SESSION['nombre'] = $nombre;
    $_SESSION['apellido'] = $apellido;
    $_SESSION['img'] = $imgper;
    $_SESSION['username'] = $username;
  	header('location: ui-login.php');
  }
}
// ... 

// LOGIN USER
if (isset($_POST['login_user'])) {
    $username = mysqli_real_escape_string($link, $_POST['username']);
    $password = mysqli_real_escape_string($link, $_POST['password']);
  
    if (empty($username)) {
        array_push($errors, "Username is required");
    }
    if (empty($password)) {
        array_push($errors, "Password is required");
    }
  
    if (count($errors) == 0) {
        $password = md5($password);
        $query = "SELECT * FROM nextia_user WHERE username='$username' AND password='$password'";
        $results = mysqli_query($link, $query);
        $arreglo = mysqli_fetch_array($results);


        $id = $arreglo[0];
        $email = $arreglo[3];
        $nombre = $arreglo[5];
        $apellido = $arreglo[6];
        $img = $arreglo[7];


        if (mysqli_num_rows($results) == 1) {
          $_SESSION['id'] = $id;
          $_SESSION['email'] = $email;
          $_SESSION['nombre'] = $nombre;
          $_SESSION['apellido'] = $apellido;
          $_SESSION['img'] = $img;
          $_SESSION['username'] = $username;
          $_SESSION['success'] = "You are now logged in";
          $_SESSION['fecha'] =   date('Y-m-d', strtotime('-1 days'));
          header('location: index.php');
        }else {
            array_push($errors, "Wrong username/password combination");
        }
    }
  }
    if (isset($_POST['fecha'])) {
    $fecha  = mysqli_real_escape_string($link, $_POST['fech-actual']);
    $_SESSION['fecha'] =  $fecha;
    #echo '<p>'.'Semana seleccionada: '.$_SESSION['fecha'].'</p>';

    }
    

  $n = 'Narrative Tech ';


  
?>
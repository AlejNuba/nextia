<?php
include('header.inc');
?> 
        <!-- START CONTAINER -->
        <div class="page-container row-fluid">

            <!-- SIDEBAR - START -->
<?php
include('sidebar.inc');
?>             <!-- START CONTENT -->
            <section id="main-content" class=" ">
                <section class="wrapper main-wrapper" style=''>


                    <div class='col-xl-12 col-lg-12 col-md-12 col-12'>
                        <div class="page-title">

                            <div class="float-left">
                                <h1 class="title">Statistics Reports</h1>                            </div>

                            <div class="float-right d-none">
                                <ol class="breadcrumb">
                                    <li>
                                        <a href="index.html"><i class="fa fa-home"></i>Home</a>
                                    </li>
                                    <li>
                                        <a href="soc-report-site.html">Reports</a>
                                    </li>
                                    <li class="active">
                                        <strong>Statistics</strong>
                                    </li>
                                </ol>
                            </div>

                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="row">
                        <div class="col-xl-4 col-lg-6 col-md-12 col-12">
                            <section class="box ">
                                <header class="panel_header">
                                    <h2 class="title float-left">Line</h2>
                                    <div class="actions panel_actions float-right">
                                        <i class="box_toggle fa fa-chevron-down"></i>
                                        <i class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></i>
                                        <i class="box_close fa fa-times"></i>
                                    </div>
                                </header>
                                <div class="content-body">        <div class="row">
                                        <div class="col-lg-12 col-md-12 col-12">
                                            <span class="inlinesparkline">Loading..</span>
                                        </div>
                                    </div>
                                </div>
                            </section>    </div>

                        <div class="col-xl-4 col-lg-6 col-md-12 col-12">
                            <section class="box ">
                                <header class="panel_header">
                                    <h2 class="title float-left">Bar</h2>
                                    <div class="actions panel_actions float-right">
                                        <i class="box_toggle fa fa-chevron-down"></i>
                                        <i class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></i>
                                        <i class="box_close fa fa-times"></i>
                                    </div>
                                </header>
                                <div class="content-body">        <div class="row">
                                        <div class="col-lg-12 col-md-12 col-12">
                                            <span class="dynamicbar">Loading..</span>
                                        </div>
                                    </div>
                                </div>
                            </section>    </div>

                        <div class="col-xl-4 col-lg-6 col-md-12 col-12">
                            <section class="box ">
                                <header class="panel_header">
                                    <h2 class="title float-left">Negative Bar</h2>
                                    <div class="actions panel_actions float-right">
                                        <i class="box_toggle fa fa-chevron-down"></i>
                                        <i class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></i>
                                        <i class="box_close fa fa-times"></i>
                                    </div>
                                </header>
                                <div class="content-body">        <div class="row">
                                        <div class="col-lg-12 col-md-12 col-12">
                                            <span class="negativebar">Loading...</span>
                                        </div>
                                    </div>
                                </div>
                            </section>    </div>


                        <div class="col-xl-4 col-lg-6 col-md-12 col-12">
                            <section class="box ">
                                <header class="panel_header">
                                    <h2 class="title float-left">Stacked Bar</h2>
                                    <div class="actions panel_actions float-right">
                                        <i class="box_toggle fa fa-chevron-down"></i>
                                        <i class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></i>
                                        <i class="box_close fa fa-times"></i>
                                    </div>
                                </header>
                                <div class="content-body">        <div class="row">
                                        <div class="col-lg-12 col-md-12 col-12">
                                            <span class="stackedbar">1:2:2,2:4:3,4:2:4,4:5:1,4:1:2,6:6:4,2:3:2,3:2:6,2:0:2,6:2:4,7:4:2,3:4:1,3:7:4</span>
                                        </div>
                                    </div>
                                </div>
                            </section>    </div>

                        <div class="col-xl-4 col-lg-6 col-md-12 col-12">
                            <section class="box ">
                                <header class="panel_header">
                                    <h2 class="title float-left">Composite Line</h2>
                                    <div class="actions panel_actions float-right">
                                        <i class="box_toggle fa fa-chevron-down"></i>
                                        <i class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></i>
                                        <i class="box_close fa fa-times"></i>
                                    </div>
                                </header>
                                <div class="content-body">        <div class="row">
                                        <div class="col-lg-12 col-md-12 col-12">
                                            <span id="compositeline">Loading...</span>
                                        </div>
                                    </div>
                                </div>
                            </section>    </div>

                        <div class="col-xl-4 col-lg-6 col-md-12 col-12">
                            <section class="box ">
                                <header class="panel_header">
                                    <h2 class="title float-left">Normal Range</h2>
                                    <div class="actions panel_actions float-right">
                                        <i class="box_toggle fa fa-chevron-down"></i>
                                        <i class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></i>
                                        <i class="box_close fa fa-times"></i>
                                    </div>
                                </header>
                                <div class="content-body">        <div class="row">
                                        <div class="col-lg-12 col-md-12 col-12">
                                            <span id="normalline">Loading...</span>
                                        </div>
                                    </div>
                                </div>
                            </section>    </div>

                        <div class="col-xl-4 col-lg-6 col-md-12 col-12">
                            <section class="box ">
                                <header class="panel_header">
                                    <h2 class="title float-left">Composite Bar</h2>
                                    <div class="actions panel_actions float-right">
                                        <i class="box_toggle fa fa-chevron-down"></i>
                                        <i class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></i>
                                        <i class="box_close fa fa-times"></i>
                                    </div>
                                </header>
                                <div class="content-body">        <div class="row">
                                        <div class="col-lg-12 col-md-12 col-12">
                                            <span id="compositebar">Loading...</span>
                                        </div>
                                    </div>
                                </div>
                            </section>    </div>


                        <div class="col-xl-4 col-lg-6 col-md-12 col-12">
                            <section class="box ">
                                <header class="panel_header">
                                    <h2 class="title float-left">Discrete</h2>
                                    <div class="actions panel_actions float-right">
                                        <i class="box_toggle fa fa-chevron-down"></i>
                                        <i class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></i>
                                        <i class="box_close fa fa-times"></i>
                                    </div>
                                </header>
                                <div class="content-body">        <div class="row">
                                        <div class="col-lg-12 col-md-12 col-12">
                                            <span class="discrete1">Loading...</span>
                                        </div>
                                    </div>
                                </div>
                            </section>    </div>


                        <div class="col-xl-4 col-lg-6 col-md-12 col-12">
                            <section class="box ">
                                <header class="panel_header">
                                    <h2 class="title float-left">Tristate</h2>
                                    <div class="actions panel_actions float-right">
                                        <i class="box_toggle fa fa-chevron-down"></i>
                                        <i class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></i>
                                        <i class="box_close fa fa-times"></i>
                                    </div>
                                </header>
                                <div class="content-body">        <div class="row">
                                        <div class="col-lg-12 col-md-12 col-12">
                                            <span class="sparktristate">Loading...</span>
                                        </div>
                                    </div>
                                </div>
                            </section>    </div>


                        <div class="col-xl-4 col-lg-6 col-md-12 col-12">
                            <section class="box ">
                                <header class="panel_header">
                                    <h2 class="title float-left">Box Plot</h2>
                                    <div class="actions panel_actions float-right">
                                        <i class="box_toggle fa fa-chevron-down"></i>
                                        <i class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></i>
                                        <i class="box_close fa fa-times"></i>
                                    </div>
                                </header>
                                <div class="content-body">        <div class="row">
                                        <div class="col-lg-12 col-md-12 col-12">
                                            <span class="sparkboxplot">Loading...</span>
                                        </div>
                                    </div>
                                </div>
                            </section>    </div>

                        <div class="col-xl-4 col-lg-6 col-md-12 col-12">
                            <section class="box ">
                                <header class="panel_header">
                                    <h2 class="title float-left">Pie</h2>
                                    <div class="actions panel_actions float-right">
                                        <i class="box_toggle fa fa-chevron-down"></i>
                                        <i class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></i>
                                        <i class="box_close fa fa-times"></i>
                                    </div>
                                </header>
                                <div class="content-body">        <div class="row">
                                        <div class="col-lg-12 col-md-12 col-12">
                                            <span class="sparkpie">Loading...</span>
                                        </div>
                                    </div>
                                </div>
                            </section>    </div>

                        <div class="col-xl-4 col-lg-6 col-md-12 col-12">
                            <section class="box ">
                                <header class="panel_header">
                                    <h2 class="title float-left">Bullet</h2>
                                    <div class="actions panel_actions float-right">
                                        <i class="box_toggle fa fa-chevron-down"></i>
                                        <i class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></i>
                                        <i class="box_close fa fa-times"></i>
                                    </div>
                                </header>
                                <div class="content-body">        <div class="row">
                                        <div class="col-lg-12 col-md-12 col-12">
                                            <span class="sparkbullet">Loading...</span>
                                        </div>
                                    </div>
                                </div>
                            </section>    </div>
                    </div>


                </section>
            </section>
            <!-- END CONTENT -->

<?php
include('chatapi.inc');
?>   </div>
        <!-- END CONTAINER -->

<?php
include('footer.inc');
?>  
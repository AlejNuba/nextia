<?php
include('header.inc');
?>      
        <!-- START CONTAINER -->
        <div class="page-container row-fluid">

            <!-- SIDEBAR - START -->
<?php
include('sidebar.inc');
?>  
            <!-- START CONTENT -->
            <section id="main-content" class=" ">
                <section class="wrapper main-wrapper" style=''>

                    <div class='col-xl-12 col-lg-12 col-md-12 col-12'>
                        <div class="page-title">

                            <div class="float-left">
                                <h1 class="title">User Profile</h1>                            </div>

                            <div class="float-right d-none">
                                <ol class="breadcrumb">
                                    <li>
                                        <a href="index.html"><i class="fa fa-home"></i>Home</a>
                                    </li>
                                    <li>
                                        <a href="ui-pricing.html">Pages</a>
                                    </li>
                                    <li class="active">
                                        <strong>Profile</strong>
                                    </li>
                                </ol>
                            </div>

                        </div>
                    </div>
                    <div class="clearfix"></div>


                    <div class="col-xl-12">
                        <section class="box nobox">
                            <div class="content-body">    <div class="row">
                                    <div class="col-lg-3 col-md-4 col-12">
                                        <div class="uprofile-image">
                                            <img src="data/profile/spock.jpeg" class="img-fluid">
                                        </div>
                                        <div class="uprofile-name">
                                            <h3>
                                                <a href="#">Spock</a>
                                                <!-- Available statuses: online, idle, busy, away and offline -->
                                                <span class="uprofile-status online"></span>
                                            </h3>
                                            <p class="uprofile-title">Web Developer</p>
                                        </div>
                                        <div class="uprofile-info">
                                            <ul class="list-unstyled">
                                                <li><i class='fa fa-home'></i> Chicago, USA</li>
                                                <li><i class='fa fa-user'></i> -2 Contacts</li>
                                                <li><i class='fa fa-suitcase'></i> Tech Lead, Televisco</li>
                                            </ul>
                                        </div>
                                        <div class="uprofile-buttons">
                                            <a class="btn btn-md btn-primary">Send Message</a>
                                            <a class="btn btn-md btn-primary">Add as Friend</a>
                                        </div>
                                        <div class=" uprofile-social">

                                            <a href="#" class="btn btn-primary btn-md facebook"><i class="fa fa-facebook icon-xs"></i></a>
                                            <a href="#" class="btn btn-primary btn-md twitter"><i class="fa fa-twitter icon-xs"></i></a>
                                            <a href="#" class="btn btn-primary btn-md google-plus"><i class="fa fa-google-plus icon-xs"></i></a>
                                            <a href="#" class="btn btn-primary btn-md dribbble"><i class="fa fa-dribbble icon-xs"></i></a>

                                        </div> 

                                    </div>
                                    <div class="col-lg-9 col-md-8 col-12">

                                        <div class="uprofile-content">
                                            <div class="enter_post col-lg-12 col-md-12 col-12">

                                                <div class="form-group">
                                                    <div class="controls">
                                                        <textarea class="form-control autogrow" id="field-7"  placeholder="What's on your mind?"></textarea>
                                                    </div>
                                                </div>
                                                <div class="enter_post_btns col-lg-12 col-md-12 col-12">
                                                    <a href="#" class="btn btn-md float-right btn-primary">Post</a>
                                                    <a href="#" class="btn btn-md float-right btn-link"><i class="fa fa-image"></i></a>
                                                    <a href="#" class="btn btn-md float-right btn-link"><i class="fa fa-map-marker"></i></a>
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>






                                            <div class="uprofile_wall_posts col-md-12 col-sm-12 col-xs-12">
                                                <div class="pic-wrapper col-md-1 col-sm-1 col-xs-2 text-center">
                                                    <img src="data/profile/avatar-2.png" class="" alt="">
                                                </div>
                                                <div class="info-wrapper col-md-11 col-sm-11 col-xs-10">                    
                                                    <div class="username">
                                                        <span class="bold">John Smith</span> post in group <span class="bold">work</span>   
                                                    </div>
                                                    <div class="info text-muted">
                                                        "Balance" is a concept based on human perception and the complex nature of the human senses of weight and proportion. Humans can evaluate these visual elements in several situations to find a sense of balance.
                                                    </div>  
                                                    <div class="info-details">
                                                        <ul class="list-unstyled list-inline">
                                                            <li><a href="#" class="text-muted">15 Minutes ago</a></li>
                                                            <li><a href="#" class="text-muted"><i class="fa fa-comment"></i> 584</a></li>
                                                            <li><a href="#" class="text-orange"><i class="fa fa-heart"></i> 12k</a></li>
                                                            <li><a href="#" class="text-info"><i class="fa fa-reply"></i> Reply</a></li>
                                                            <li><a href="#" class="text-warning"><i class="fa fa-star"></i> Favourite</a></li>
                                                            <li><a href="#" class="text-muted">More</a></li>
                                                        </ul>

                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="comment">
                                                        <div class="pic-wrapper col-md-1 col-sm-1 col-xs-2 text-center">
                                                            <img data-src-retina="data/profile/avatar-3.png" data-src="data/profile/avatar-3.png" src="data/profile/avatar-3.png" alt="">
                                                        </div>
                                                        <div class="info-wrapper col-md-11 col-sm-11 col-xs-10">                    
                                                            <div class="username">
                                                                <span class="bold">Fin</span> 
                                                            </div>
                                                            <div class="info text-muted">
                                                                Perfect info for the project. Great work :)
                                                            </div>  
                                                            <div class="info-details">
                                                                <ul class="list-unstyled list-inline">
                                                                    <li><a href="#" class="text-muted">10 Minutes ago</a></li>
                                                                    <li><a href="#" class="text-orange"><i class="fa fa-heart-o"></i> Like</a></li>
                                                                    <li><a href="#" class="text-muted">More</a></li>
                                                                </ul>
                                                            </div>

                                                        </div>  
                                                        <div class="clearfix"></div>                        
                                                    </div>

                                                    <div class="clearfix"></div>
                                                    <div class="comment">
                                                        <div class="pic-wrapper col-md-1 col-sm-1 col-xs-2 text-center">
                                                            <img data-src-retina="data/profile/avatar-4.png" data-src="data/profile/avatar-4.png" src="data/profile/avatar-4.png" alt="">
                                                        </div>
                                                        <div class="info-wrapper col-md-11 col-sm-11 col-xs-10">                    
                                                            <div class="username">
                                                                <span class="bold">Arun</span> 
                                                            </div>
                                                            <div class="info text-muted">
                                                                Keep it up. Much appreciated effort.
                                                            </div>  
                                                            <div class="info-details">
                                                                <ul class="list-unstyled list-inline">
                                                                    <li><a href="#" class="text-muted">8 Minutes ago</a></li>
                                                                    <li><a href="#" class="text-orange"><i class="fa fa-heart"></i> Liked</a></li>
                                                                    <li><a href="#" class="text-muted">More</a></li>
                                                                </ul>
                                                            </div>

                                                        </div>  
                                                        <div class="clearfix"></div>                        
                                                    </div>

                                                    <div class="comment comment-input">                         

                                                        <div class="pic-wrapper col-md-1 col-sm-1 col-xs-2 text-center">
                                                            <img data-src-retina="data/profile/profile.png" data-src="data/profile/profile.png" src="data/profile/profile.png" alt="">
                                                        </div>
                                                        <div class="info-wrapper col-md-11 col-sm-11 col-xs-10">                    
                                                            <div class="input-group primary  col-md-6">
                                                                <input type="text" class="form-control" placeholder="Post a comment">
                                                                <span class="input-group-addon">
                                                                    <i class="fa fa-rocket"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>  
                                                <div class="clearfix"></div>                        
                                            </div>



                                            <div class="uprofile_wall_posts col-md-12 col-sm-12 col-xs-12">
                                                <div class="pic-wrapper col-md-1 col-sm-1 col-xs-2 text-center">
                                                    <img src="data/profile/avatar-1.png" class="" alt="">
                                                </div>
                                                <div class="info-wrapper col-md-11 col-sm-11 col-xs-10">                    
                                                    <div class="username">
                                                        <span class="bold">John Smith</span> post in group <span class="bold">work</span>   
                                                    </div>
                                                    <div class="info text-muted">
                                                        "Balance" is a concept based on human perception and the complex nature of the human senses of weight and proportion. Humans can evaluate these visual elements in several situations to find a sense of balance.
                                                    </div>  
                                                    <div class="info-details">
                                                        <ul class="list-unstyled list-inline">
                                                            <li><a href="#" class="text-muted">15 Minutes ago</a></li>
                                                            <li><a href="#" class="text-muted"><i class="fa fa-comment"></i> 584</a></li>
                                                            <li><a href="#" class="text-orange"><i class="fa fa-heart"></i> 12k</a></li>
                                                            <li><a href="#" class="text-info"><i class="fa fa-reply"></i> Reply</a></li>
                                                            <li><a href="#" class="text-warning"><i class="fa fa-star"></i> Favourite</a></li>
                                                            <li><a href="#" class="text-muted">More</a></li>
                                                        </ul>

                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="comment">
                                                        <div class="pic-wrapper col-md-1 col-sm-1 col-xs-2 text-center">
                                                            <img data-src-retina="data/profile/avatar-2.png" data-src="data/profile/avatar-2.png" src="data/profile/avatar-2.png" alt="">
                                                        </div>
                                                        <div class="info-wrapper col-md-11 col-sm-11 col-xs-10">                    
                                                            <div class="username">
                                                                <span class="bold">Fin</span> 
                                                            </div>
                                                            <div class="info text-muted">
                                                                Perfect info for the project. Great work :)
                                                            </div>  
                                                            <div class="info-details">
                                                                <ul class="list-unstyled list-inline">
                                                                    <li><a href="#" class="text-muted">10 Minutes ago</a></li>
                                                                    <li><a href="#" class="text-orange"><i class="fa fa-heart-o"></i> Like</a></li>
                                                                    <li><a href="#" class="text-muted">More</a></li>
                                                                </ul>
                                                            </div>

                                                        </div>  
                                                        <div class="clearfix"></div>                        
                                                    </div>

                                                    <div class="clearfix"></div>
                                                    <div class="comment">
                                                        <div class="pic-wrapper col-md-1 col-sm-1 col-xs-2 text-center">
                                                            <img data-src-retina="data/profile/avatar-3.png" data-src="data/profile/avatar-3.png" src="data/profile/avatar-3.png" alt="">
                                                        </div>
                                                        <div class="info-wrapper col-md-11 col-sm-11 col-xs-10">                    
                                                            <div class="username">
                                                                <span class="bold">Arun</span> 
                                                            </div>
                                                            <div class="info text-muted">
                                                                Keep it up. Much appreciated effort.
                                                            </div>  
                                                            <div class="info-details">
                                                                <ul class="list-unstyled list-inline">
                                                                    <li><a href="#" class="text-muted">8 Minutes ago</a></li>
                                                                    <li><a href="#" class="text-orange"><i class="fa fa-heart"></i> Liked</a></li>
                                                                    <li><a href="#" class="text-muted">More</a></li>
                                                                </ul>
                                                            </div>

                                                        </div>  
                                                        <div class="clearfix"></div>                        
                                                    </div>

                                                    <div class="comment comment-input">                         

                                                        <div class="pic-wrapper col-md-1 col-sm-1 col-xs-2 text-center">
                                                            <img data-src-retina="data/profile/profile.png" data-src="data/profile/profile.png" src="data/profile/profile.png" alt="">
                                                        </div>
                                                        <div class="info-wrapper col-md-11 col-sm-11 col-xs-10">                    
                                                            <div class="input-group primary  col-md-6">
                                                                <input type="text" class="form-control" placeholder="Post a comment">
                                                                <span class="input-group-addon">
                                                                    <i class="fa fa-rocket"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>  
                                                <div class="clearfix"></div>                        
                                            </div>


                                        </div>









                                    </div>
                                </div>
                            </div>
                        </section></div>


                </section>
            </section>
            <!-- END CONTENT -->
            <div class="page-chatapi hideit">

                <div class="search-bar">
                    <input type="text" placeholder="Search" class="form-control">
                </div>

                <div class="chat-wrapper">
                    <h4 class="group-head">Groups</h4>
                    <ul class="group-list list-unstyled">
                        <li class="group-row">
                            <div class="group-status available">
                                <i class="fa fa-circle"></i>
                            </div>
                            <div class="group-info">
                                <h4><a href="#">Work</a></h4>
                            </div>
                        </li>
                        <li class="group-row">
                            <div class="group-status away">
                                <i class="fa fa-circle"></i>
                            </div>
                            <div class="group-info">
                                <h4><a href="#">Friends</a></h4>
                            </div>
                        </li>

                    </ul>


                    <h4 class="group-head">Favourites</h4>
                    <ul class="contact-list">

                        <li class="user-row" id='chat_user_1' data-user-id='1'>
                            <div class="user-img">
                                <a href="#"><img src="data/profile/avatar-1.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Clarine Vassar</a></h4>
                                <span class="status available" data-status="available"> Available</span>
                            </div>
                            <div class="user-status available">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>
                        <li class="user-row" id='chat_user_2' data-user-id='2'>
                            <div class="user-img">
                                <a href="#"><img src="data/profile/avatar-2.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Brooks Latshaw</a></h4>
                                <span class="status away" data-status="away"> Away</span>
                            </div>
                            <div class="user-status away">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>
                        <li class="user-row" id='chat_user_3' data-user-id='3'>
                            <div class="user-img">
                                <a href="#"><img src="data/profile/avatar-3.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Clementina Brodeur</a></h4>
                                <span class="status busy" data-status="busy"> Busy</span>
                            </div>
                            <div class="user-status busy">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>

                    </ul>


                    <h4 class="group-head">More Contacts</h4>
                    <ul class="contact-list">

                        <li class="user-row" id='chat_user_4' data-user-id='4'>
                            <div class="user-img">
                                <a href="#"><img src="data/profile/avatar-4.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Carri Busey</a></h4>
                                <span class="status offline" data-status="offline"> Offline</span>
                            </div>
                            <div class="user-status offline">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>
                        <li class="user-row" id='chat_user_5' data-user-id='5'>
                            <div class="user-img">
                                <a href="#"><img src="data/profile/avatar-5.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Melissa Dock</a></h4>
                                <span class="status offline" data-status="offline"> Offline</span>
                            </div>
                            <div class="user-status offline">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>
                        <li class="user-row" id='chat_user_6' data-user-id='6'>
                            <div class="user-img">
                                <a href="#"><img src="data/profile/avatar-1.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Verdell Rea</a></h4>
                                <span class="status available" data-status="available"> Available</span>
                            </div>
                            <div class="user-status available">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>
                        <li class="user-row" id='chat_user_7' data-user-id='7'>
                            <div class="user-img">
                                <a href="#"><img src="data/profile/avatar-2.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Linette Lheureux</a></h4>
                                <span class="status busy" data-status="busy"> Busy</span>
                            </div>
                            <div class="user-status busy">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>
                        <li class="user-row" id='chat_user_8' data-user-id='8'>
                            <div class="user-img">
                                <a href="#"><img src="data/profile/avatar-3.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Araceli Boatright</a></h4>
                                <span class="status away" data-status="away"> Away</span>
                            </div>
                            <div class="user-status away">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>
                        <li class="user-row" id='chat_user_9' data-user-id='9'>
                            <div class="user-img">
                                <a href="#"><img src="data/profile/avatar-4.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Clay Peskin</a></h4>
                                <span class="status busy" data-status="busy"> Busy</span>
                            </div>
                            <div class="user-status busy">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>
                        <li class="user-row" id='chat_user_10' data-user-id='10'>
                            <div class="user-img">
                                <a href="#"><img src="data/profile/avatar-5.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Loni Tindall</a></h4>
                                <span class="status away" data-status="away"> Away</span>
                            </div>
                            <div class="user-status away">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>
                        <li class="user-row" id='chat_user_11' data-user-id='11'>
                            <div class="user-img">
                                <a href="#"><img src="data/profile/avatar-1.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Tanisha Kimbro</a></h4>
                                <span class="status idle" data-status="idle"> Idle</span>
                            </div>
                            <div class="user-status idle">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>
                        <li class="user-row" id='chat_user_12' data-user-id='12'>
                            <div class="user-img">
                                <a href="#"><img src="data/profile/avatar-2.png" alt=""></a>
                            </div>
                            <div class="user-info">
                                <h4><a href="#">Jovita Tisdale</a></h4>
                                <span class="status idle" data-status="idle"> Idle</span>
                            </div>
                            <div class="user-status idle">
                                <i class="fa fa-circle"></i>
                            </div>
                        </li>

                    </ul>
                </div>

            </div>


            <div class="chatapi-windows ">


            </div>    </div>
        <!-- END CONTAINER -->
        <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->


        <!-- CORE JS FRAMEWORK - START --> 
        <script src="assets/js/jquery-3.2.1.min.js" type="text/javascript"></script> 
        <script src="assets/js/popper.min.js" type="text/javascript"></script> 
        <!-- <script src="assets/js/jquery.easing.min.js" type="text/javascript"></script>  -->
        <script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>  

        <script src="assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js" type="text/javascript"></script> 
        <script src="assets/plugins/viewport/viewportchecker.js" type="text/javascript"></script>  
        <!-- CORE JS FRAMEWORK - END --> 


        <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
        <script src="assets/plugins/autosize/autosize.min.js" type="text/javascript"></script><!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


        <!-- CORE TEMPLATE JS - START --> 
        <script src="assets/js/scripts.js" type="text/javascript"></script> 
        <!-- END CORE TEMPLATE JS - END --> 

        <!-- Sidebar Graph - START --> 
        <script src="assets/plugins/sparkline-chart/jquery.sparkline.min.js" type="text/javascript"></script>
        <script src="assets/js/chart-sparkline.js" type="text/javascript"></script>
        <!-- Sidebar Graph - END --> 













        <!-- General section box modal start -->
        <div class="modal" id="section-settings" tabindex="-1" role="dialog" aria-labelledby="ultraModal-Label" aria-hidden="true">
            <div class="modal-dialog animated bounceInDown">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Section Settings</h4>
                    </div>
                    <div class="modal-body">

                        Body goes here...

                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                        <button class="btn btn-success" type="button">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- modal end -->
    </body>
</html>




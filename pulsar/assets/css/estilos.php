<?php 
    header('Content-type: text/css');
    include('../../../config/configWS.php');

?>

.loginpage h1 a {
    background-image: url("../images/<?php echo $company_loginlogo ?>") !important;
}

.page-topbar .logo-area {

    background-color: <?php echo $company_logoareabgcolor ?> !important;

}

.page-topbar .logo-area {
    background-image: url('../images/<?php echo $company_logo ?>')!important;

}

.page-topbar.sidebar_shift .logo-area {
    background-image: url(../images/<?php echo $company_logofolded ?>) !important ;

}

.page-topbar.chat_shift .logo-area {
    background-image: url('../images/<?php echo $company_logofolded ?>') !important ;

}

@media (max-width: 991px) {

}

@media (max-width: 767px) {
    .page-topbar .logo-area {
        background-image: url('../images/<?php echo $company_logofolded ?>') !important ;
    
    }
    
}

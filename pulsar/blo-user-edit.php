<?php
include('header.inc');

include('useraccountinfo.php');

?>
        <!-- START CONTAINER -->
        <div class="page-container row-fluid">

            <!-- SIDEBAR - START -->
            <?php
            include('sidebar.inc');
            ?>   
            <!--  SIDEBAR - END -->
            <!-- START CONTENT -->
            <section id="main-content" class=" ">
                <section class="wrapper main-wrapper" style=''>

                    <div class='col-xl-12 col-lg-12 col-md-12 col-12'>
                        <div class="page-title">

                            <div class="float-left">
                                <h1 class="title">Edit User</h1>                            </div>

                            <div class="float-right d-none">
                                <ol class="breadcrumb">
                                    <li>
                                        <a href="index.html"><i class="fa fa-home"></i>Home</a>
                                    </li>
                                    <li>
                                        <a href="blo-users.html">Users</a>
                                    </li>
                                    <li class="active">
                                        <strong>Edit User</strong>
                                    </li>
                                </ol>
                            </div>

                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-xl-12 col-lg-12 col-12 col-md-12">
                        <section class="box ">
                            <header class="panel_header">
                                <h2 class="title float-left">Basic Info</h2>
                                <div class="actions panel_actions float-right">
                                    <i class="box_toggle fa fa-chevron-down"></i>
                                    <i class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></i>
                                    <i class="box_close fa fa-times"></i>
                                </div>
                            </header>
                            <div class="content-body">
                                <div class="row">
                                    <form name="loginform" id="loginform" action="blo-user-edit.php" enctype="multipart/form-data" method="post" >
                                    <?php include('errors.php'); ?>
                                    
                                        <div class="col-xl-8 col-lg-8 col-md-9 col-12">

                                            <div class="form-group">
                                                <label class="form-label" for="field-1">Username</label>
                                                <span class="desc"></span>
                                                <div class="controls">
                                                    <p><?php echo $_SESSION['username'];?></p>
                                                    <input name="username" type="text" value="" class="form-control" id="">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="form-label" for="field-1">Name</label>
                                                <span class="desc"></span>
                                                <div class="controls">
                                                    <p><?php echo $_SESSION['nombre'];?></p>
                                                    <input name="nombre" type="text" value="" class="form-control" id="">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="form-label" for="field-1">Last Name</label>
                                                <span class="desc"></span>
                                                <div class="controls">
                                                    <p><?php echo $_SESSION['apellido'];?></p>
                                                    <input name="apellido" type="text" value="" class="form-control" id="">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="form-label" for="imagen">Profile Image</label>
                                                <span class="desc"></span>
                                                <img class="img-fluid" src="data/profile/<?php echo $_SESSION['img']; ?>" alt="" style="max-width:120px;">
                                                <div class="controls">
                                                    <input id="imagen" name="imagen" size="30" type="file" class="form-control" >
                                                </div>
                                            </div>

                                            </form> 

                                            <div class="col-xl-8 col-lg-8 col-md-9 col-12 padding-bottom-30">
                                                <div class="text-left">
                                                    <input type="submit" name="datos_b" id="wp-submit" class="btn btn-primary" value="Guardar" />
                                                    <button type="button" class="btn">Cancel</button>
                                                </div>
                                            </div>
                                    </form>
                                </div>


                            </div>
                        </section></div>


                    <div class="col-xl-12 col-lg-12 col-12 col-md-12">
                        <section class="box ">
                            <header class="panel_header">
                                <h2 class="title float-left">User Account Info</h2>
                                <div class="actions panel_actions float-right">
                                    <i class="box_toggle fa fa-chevron-down"></i>
                                    <i class="box_setting fa fa-cog" data-toggle="modal" href="#section-settings"></i>
                                    <i class="box_close fa fa-times"></i>
                                </div>
                            </header>
                            <div class="content-body">
                                <div class="row">
                                    <form name="loginform" id="loginform" action="blo-user-edit.php" method="post">
                                    <?php include('errors.php'); ?>
                                        <div class="col-xl-8 col-lg-8 col-md-9 col-12">

                                            <div class="form-group">
                                                <label class="form-label" for="field-1">Email</label>
                                                <span class="desc"></span>
                                                <div class="controls">
                                                    <p><?php echo $_SESSION['email']; ?></p>
                                                    <input id="email" name="email"  type="email" value="" class="form-control" id="field-3">
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="form-label" for="field-2">Password</label>
                                                <span class="desc"></span>
                                                <div class="controls">
                                                    <input id="password_1" name="password_1" type="password" value="" class="form-control" id="field-2">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label" for="field-2">Confirm Password</label>
                                                <span class="desc"></span>
                                                <div class="controls">
                                                    <input id="password_2" name="password_2" type="password"  value="" class="form-control" id="field-21">
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-xl-8 col-lg-8 col-md-9 col-12 padding-bottom-30">
                                            <div class="text-left">
                                                <input type="submit" name="datos_p" id="wp-submit" class="btn btn-primary" value="Guardar" />
                                                <button type="button" class="btn">Cancel</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>


                            </div>
                        </section></div>


                </section>
            </section>
            <!-- END CONTENT -->
        <?php
        include('chatapi.inc');
        ?>   </div>
        <!-- END CONTAINER -->
        <!-- END CONTAINER -->
        <?php
        include('footer.inc');
        ?>  
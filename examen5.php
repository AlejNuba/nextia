<?php
  function burbuja($array, $n) {
    for ($i = 1; $i < $n; $i++) {
      for ($j = 0; $j < $n - $i; $j++) {
        if ($array[$j] > $array[$j + 1]) {
          $k = $array[$j + 1]; 
          $array[$j + 1] = $array[$j]; 
          $array[$j] = $k;
        }
      }
    }
    
    return $array;
  }
  
  $array = array(5, 4, 8, 9, 4, 2, 4, 6, 4, 1, 7);
  $bubble = burbuja($array, count($array));
  
  echo "<p>Input</p>";

  for ($i = 0; $i < count($array); $i++) {
    echo $array[$i] ." ";
  }

  echo "<p>Output</p>";

  for ($i = 0; $i < count($bubble); $i++) {
    echo $bubble[$i] ." ";
  }

?>